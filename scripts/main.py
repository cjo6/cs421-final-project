import sys
import os

# Add the parent directory of src to the system path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))
from grammar_reader import GrammarReader
from lr_parser import LRParser


if __name__ == "__main__":
    grammar_reader = GrammarReader()
    parser = LRParser(grammar_reader)
    parser.run()
