class FollowSets:
    """Class to compute the follow sets of a given grammar"""
    def __init__(self, grammar):
        self.grammar = grammar
        self.first_sets = self._compute_first_sets()
        self.follow_sets = self._compute_follow_sets()

    # We use the first set to help determine the follow set, so need to compute this as well
    def _compute_first_sets(self):
        first_set = {non_terminal: set() for non_terminal in self.grammar}
        changed = True

        while changed:
            changed = False
            for non_terminal in self.grammar:
                for production in self.grammar[non_terminal]:
                    i = 0
                    # Need to scan through the entire production
                    while i < len(production):
                        symbol = production[i]
                        if symbol not in self.grammar:  # Terminal
                            if symbol not in first_set[non_terminal]:
                                first_set[non_terminal].add(symbol)
                                changed = True
                            break
                        else:  # Non-terminal
                            before_update = len(first_set[non_terminal])
                            first_set[non_terminal].update(first_set[symbol] - {""})
                            if "" in first_set[symbol]:
                                i += 1
                                if i == len(production):
                                    first_set[non_terminal].add("")
                            else:
                                break
                            if before_update != len(first_set[non_terminal]):
                                changed = True
        return first_set

    def _compute_follow_sets(self):
        """Compute the follow sets of a given grammar"""
        follow_set = {non_terminal: set() for non_terminal in self.grammar}
        # The S production always contains $ in its follow set
        follow_set[next(iter(self.grammar))].add("$")
        changed = True

        while changed:
            changed = False
            for non_terminal in self.grammar:
                for production in self.grammar[non_terminal]:
                    follow_temp = follow_set[non_terminal]
                    # Before we scan, we need to reverse the production, since we're looking for
                    # follow sets
                    for symbol in reversed(production):
                        if symbol in self.grammar:
                            before_update = len(follow_set[symbol])
                            follow_set[symbol].update(follow_temp)
                            if "" in self.first_sets[symbol]:
                                follow_temp = follow_temp.union(self.first_sets[symbol] - {""})
                            else:
                                follow_temp = self.first_sets[symbol]
                            if before_update != len(follow_set[symbol]):
                                changed = True
                        else:
                            follow_temp = {symbol}
        return follow_set
