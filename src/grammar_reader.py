import sys
from collections import defaultdict


class GrammarReader:
    """Read in a grammar provided by a user"""
    def __init__(self) -> None:
        self.grammar = self._read_grammar()
        self.rule_numbers = self._get_grammar_rule_nums()

    def _read_user_input(self) -> list:
        """Prompt the user to input the grammar"""
        print("Enter your grammar rules here (type 'END' on a new line to finish):")
        grammar_lines: list = []
        while True:
            line: str = input().strip()
            # We need some way to exit the grammar and continue with the rest of the program
            if line.upper() == "END":
                break
            elif "->" in line:
                grammar_lines.append(line)
            else:
                try:
                    grammar_lines[-1] = grammar_lines[-1] + " " + line
                except IndexError:
                    sys.exit(1)
        return grammar_lines

    def _parse_grammar(self, grammar_lines: list) -> defaultdict:
        grammar = defaultdict(list)
        for line in grammar_lines:
            lhs, rhs = line.split("->")
            lhs: str = lhs.strip()
            rhs_productions: list = [production.strip() for production in rhs.split("|")]
            grammar[lhs].extend(rhs_productions)
        return grammar

    def _read_grammar(self) -> list:
        """Driver function for the class"""
        # Read in the user input
        grammar_lines = self._read_user_input()
        # Parse the user input into the defaultdict structure that the rest of the code uses to 
        # represent a grammar
        parsed_grammar = self._parse_grammar(grammar_lines)
        for non_terminal in parsed_grammar:
            parsed_grammar[non_terminal] = [
                # We want to standardize the representation of the grammar, such that
                # "S->aS" and "S -> a S" are represented the same. If we don't, the
                # character-by-character parse breaks down
                "".join(prod.split()) for prod in parsed_grammar[non_terminal]
            ]
        # NOTE: If you want, you can add checks for common prefixes/left recursion here using
        # the functions below (see note above those functions).
        return parsed_grammar

    # The grammar rule numbers are used in the representation of the item sets when a reduce
    # action is used, so we store these as a class variable for easy and effiicent access
    def _get_grammar_rule_nums(self) -> dict:
        rule_nums = []
        for terminal, productions in self.grammar.items():
            for rhs in productions:
                final_production = f"{terminal}->{rhs}"
                rule_nums.append(final_production)
        return {rule: rule_nums.index(rule) for rule in rule_nums}

    # I included checks for left recursion and common prefixes as an intellectual exercise.
    # Hopefully others will be able to take these functions
    # and help themselves study how to identify these common problems.
    # NOTE: At present, the project only works successfully on grammars that do not exhibit left
    # recursion or common prefixes. The code is most of the way there when handling grammars that
    # do exhibit those problems, but unfortunatley I didn't have time to refine it to the point
    # where two different productions in a given item set shifting/going to on the same symbol
    # would shift/goto the same newly generated item set (the current code generated a new item set
    # for each production).
    def _validate_grammar(self, grammar: defaultdict) -> bool:
        left_recursion_present: bool = self._check_for_left_recursion(grammar)
        common_prefix_present: bool = self._check_for_common_prefix(grammar)
        return not (left_recursion_present or common_prefix_present)

    def _check_for_left_recursion(self, grammar: defaultdict) -> bool:
        productions_with_left_recursion = defaultdict(list)
        for nonterminal in grammar:
            productions = grammar[nonterminal]
            for production in productions:
                if production.startswith(nonterminal):
                    productions_with_left_recursion[nonterminal].extend([production])
        # Report results of check
        if productions_with_left_recursion:
            print("Left recursions detected:")
            for nonterminal, productions in productions_with_left_recursion.items():
                print(f"{nonterminal}: {productions}")
            return True
        else:
            print("No left recursions detected")
            return False

    def _check_for_common_prefix(self, grammar: defaultdict) -> bool:
        common_prefixes = {}
        for non_terminal, productions in grammar.items():
            prefixes = defaultdict(list)
            for production in productions:
                first_symbol = production.split()[0] if production.split() else production
                prefixes[first_symbol].append(production)
            common_prefixes[non_terminal] = {k: v for k, v in prefixes.items() if len(v) > 1}
        common_prefixes: dict = {k: v for k, v in common_prefixes.items() if v}
        # Report results of check
        if len(common_prefixes) > 0:
            print("Common prefixes detected in:")
            print({k: v for k, v in common_prefixes.items() if v})
            return True
        else:
            print("No common prefixes detected")
            return False