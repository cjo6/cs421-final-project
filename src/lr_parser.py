import sys
from follow_sets import FollowSets


class LRParser:
    """Class that performs the parsing and generates/outputs the item sets produced by the parse"""
    def __init__(self, grammar_reader):
        self.grammar = grammar_reader.grammar
        self.grammar_rule_numbers = grammar_reader.rule_numbers
        self.follow_sets = FollowSets(self.grammar).follow_sets

    def gen_productions_from_terminal(self, terminal: str) -> list:
        """Given a terminal symbol, get the corresponding productions from the grammar"""
        state_productions = []
        for production in self.grammar[terminal]:
            production_list: list[str] = [terminal, "->", "."] + [symbol for symbol in production]
            state_productions.append(production_list)
        return state_productions

    def get_production_sides(self, production) -> tuple:
        lhs = production[0]
        arrow_idx = production.index("->")
        rhs = production[arrow_idx + 1:]
        return lhs, rhs

    def gen_new_set(self, lhs, rhs):
        # Get current set's parser index
        original_period_idx = rhs.index(".")
        # Get the action (shift, reduce, goto) for the next character to be scanned
        next_char, action = self.get_action(rhs, original_period_idx)
        # If the parser doesn't reduce as the next step, continue parsing
        if action == "r":
            return None, action
        else:
            # Shift the parser one character forward in the grammar
            new_rhs = rhs.copy()
            new_rhs[original_period_idx], new_rhs[original_period_idx + 1] = (
                new_rhs[original_period_idx + 1],
                new_rhs[original_period_idx],
            )
            # Add the new production to the new set
            new_set = [[lhs, "->"] + new_rhs]
            # If the parser's next character is a terminal, need to add that terminal's productions
            # to the new set
            # This needs to run on every new production added to the set, so we use a while loop
            idx = 0
            while idx < len(new_set):
                production = new_set[idx]
                try:
                    new_next_char = production[production.index(".") + 1]
                except IndexError:
                    new_next_char = None
                if self.grammar[new_next_char]:
                    next_terminal_productions = self.gen_productions_from_terminal(new_next_char)
                    for next_terminal_production in next_terminal_productions:
                        if next_terminal_production not in new_set:
                            new_set.append(next_terminal_production)
                idx += 1
            return new_set, action

    def get_action(self, rhs, period_idx):
        """
        Gets the action to be performed:
        shift for a non-terminal, goto for a terminal, and reduce if the input is over
        """
        try:
            next_char = rhs[period_idx + 1]
        except IndexError:
            next_char = None

        if next_char is None:
            action = "r"
        elif self.grammar[next_char]:
            action = "g"
        else:
            action = "s"
        return next_char, action

    def process_production(self, production):
        """Generate the new set and action that would result from parsing a production"""
        lhs, rhs = self.get_production_sides(production)
        new_set, action = self.gen_new_set(lhs, rhs)
        return new_set, action

    def gen_set_list(self):
        """Take a grammar and generate the full list of item sets"""
        # Generate and add initial starting item set from S terminal
        set0 = self.gen_productions_from_terminal("S")
        set_list = [set0]

        # Parse each production in the initial starting set and keep track of the resulting new item
        # sets in a list. We use a list here because it maintains order (and we care about
        # generating the item sets in canonical order), and because we can then use that list's
        # index to later report the target item sets of the various shift/goto actions when
        # representing the item sets
        for production in set0:
            new_set_from_production, action = self.process_production(production)
            # If a new set is produced, and does not already exist in the set_list, add it
            if new_set_from_production and new_set_from_production not in set_list:
                set_list.append(new_set_from_production)

        # Once the initial starting item set has been parsed, we move through the entire set list
        # and parse the various productions therein. We use a while loop here because the parsing
        # will cause the list of item sets to grow, and we want to make sure that we parse
        # each item set in canonical order (again, new item sets are added at the back of the list)
        idx = 1
        while idx < len(set_list):
            # Each item set may have multiple productions
            for production in set_list[idx]:
                new_set_from_production, action = self.process_production(production)
                # If a new set is produced, and does not already exist in the set_list, add it
                if new_set_from_production and new_set_from_production not in set_list:
                    set_list.append(new_set_from_production)
            idx += 1
        return set_list

    def run(self) -> None:
        """
        Run the entire program by generating the set list and then outputting it to the console
        in a manner that is consistent with the CS421 class examples
        """
        # First we generate the list of all sets
        set_list = self.gen_set_list()

        # In order to represent each set correctly, we need to include the target set to which
        # a shift or goto action would move. This is difficult to do in the original scan as
        # the item sets are being generated dynamically, so we go through and fill those in
        # when we output the representation to the console
        for idx in range(len(set_list)):
            s = set_list[idx]
            print(f"I{idx}")
            # There should be a resulting shift/goto/reduce value for every production in every
            # item set
            for production in s:
                next_set, action = self.process_production(production)
                # If the input is fully scanned and we need to reduce, pull the follow set
                # from the given production's starting terminal and reduce on those symbols
                if action == "r":
                    follow_set = ",".join(self.follow_sets[production[0]])
                    base_rule = "".join([symbol for symbol in production if symbol != "."])
                    try:
                        rule_num = self.grammar_rule_numbers[base_rule]
                    except KeyError:
                        print(f"ERROR: {base_rule} not present in grammar rule numbers. Exiting")
                        sys.exit(1)
                    print(f"{' '.join(production)}   [{follow_set}: r{rule_num}]")
                else:
                    next_char = production[production.index(".") + 1]
                    target_idx = set_list.index(next_set)
                    print(f"{' '.join(production)}   [{next_char}: {action}{target_idx}]")
