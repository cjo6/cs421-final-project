import sys
import os
import unittest
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))
from collections import defaultdict
from follow_sets import FollowSets


class TestFollowSets(unittest.TestCase):
    def setUp(self):
        self.grammar = defaultdict(
            list,
            {'S': ['aS', 'bEc'], 'E': ['Fc'], 'F': ['xF', 'a']}
            )
        self.follow_sets = FollowSets(self.grammar)

    def test_follow_sets(self):
        expected_follow_sets = {
            "S": {"$"},
            "E": {"c"},
            "F": {"c"}
        }
        self.assertEqual(self.follow_sets.follow_sets, expected_follow_sets)


if __name__ == '__main__':
    unittest.main()
