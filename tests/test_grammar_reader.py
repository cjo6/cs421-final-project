import sys
import os
import unittest
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))
from collections import defaultdict
from grammar_reader import GrammarReader
from unittest.mock import patch

grammar_text = ["S -> a S | b E c", "E -> F c", "F -> x F | a"]
grammar_text2 = ["S -> a S | b E c", "E -> F c", "F -> x F | a"]
grammar_text3 = ["S -> a     S | b Ec", "E -> F  c", "F    -> x F | a"]


class TestGrammarReader(unittest.TestCase):
    @patch('grammar_reader.GrammarReader._read_user_input', return_value=grammar_text)
    def test_read_grammar1(self, mock_read_user_input):
        reader = GrammarReader()
        expected_grammar = defaultdict(
            list,
            {'S': ['aS', 'bEc'], 'E': ['Fc'], 'F': ['xF', 'a']}
            )
        self.assertEqual(reader.grammar, expected_grammar)

    @patch('grammar_reader.GrammarReader._read_user_input', return_value=grammar_text2)
    def test_read_grammar2(self, mock_read_user_input):
        reader = GrammarReader()
        expected_grammar = defaultdict(
            list,
            {'S': ['aS', 'bEc'], 'E': ['Fc'], 'F': ['xF', 'a']}
            )
        self.assertEqual(reader.grammar, expected_grammar)

    @patch('grammar_reader.GrammarReader._read_user_input', return_value=grammar_text3)
    def test_read_grammar3(self, mock_read_user_input):
        reader = GrammarReader()
        expected_grammar = defaultdict(
            list,
            {'S': ['aS', 'bEc'], 'E': ['Fc'], 'F': ['xF', 'a']}
            )
        self.assertEqual(reader.grammar, expected_grammar)


if __name__ == '__main__':
    unittest.main()
