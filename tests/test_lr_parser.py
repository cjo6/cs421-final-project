import sys
import os
import unittest

# Add the parent directory of src to the system path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))
from lr_parser import LRParser
from grammar_reader import GrammarReader
from unittest.mock import patch

grammar_text = ["S -> a S | b E c", "E -> F c", "F -> x F | a"]


class TestLRParser(unittest.TestCase):
    @patch('grammar_reader.GrammarReader._read_user_input', return_value=grammar_text)
    def setUp(self, mock_read_user_input):
        self.reader = GrammarReader()
        self.parser = LRParser(self.reader)

    def test_gen_productions_from_terminal(self):
        expected_productions = [['S', '->', '.', 'a', 'S'], ['S', '->', '.', 'b', 'E', 'c']]
        self.assertEqual(self.parser.gen_productions_from_terminal("S"), expected_productions)

    def test_gen_production_sides(self):
        production = ['S', '->', '.', 'a', 'S']
        expected_sides = ('S', ['.', 'a', 'S'])
        self.assertEqual(self.parser.get_production_sides(production), expected_sides)

    def test_gen_new_set(self):
        lhs = 'S'
        rhs = ['.', 'a', 'S']
        expected_new_set = (
            [['S', '->', 'a', '.', 'S'],
             ['S', '->', '.', 'a', 'S'],
             ['S', '->', '.', 'b', 'E', 'c']],
            's'
            )
        self.assertEqual(self.parser.gen_new_set(lhs, rhs), expected_new_set)

    def test_get_action(self):
        rhs = ['.', 'a', 'S']
        idx = 0
        expected_action = ("a", "s")
        self.assertEqual(self.parser.get_action(rhs, idx), expected_action)

    def test_process_production(self):
        production = ['S', '->', '.', 'a', 'S']
        expected_new_set = (
            [['S', '->', 'a', '.', 'S'],
             ['S', '->', '.', 'a', 'S'],
             ['S', '->', '.', 'b', 'E', 'c']],
            's'
            )
        self.assertEqual(self.parser.process_production(production), expected_new_set)

    def test_gen_set_list(self):
        expected_set_list = [
            [['S', '->', '.', 'a', 'S'], ['S', '->', '.', 'b', 'E', 'c']],
            [['S', '->', 'a', '.', 'S'], ['S', '->', '.', 'a', 'S'], ['S', '->', '.', 'b', 'E', 'c']],
            [['S', '->', 'b', '.', 'E', 'c'], ['E', '->', '.', 'F', 'c'], ['F', '->', '.', 'x', 'F'], ['F', '->', '.', 'a']],
            [['S', '->', 'a', 'S', '.']],
            [['S', '->', 'b', 'E', '.', 'c']],
            [['E', '->', 'F', '.', 'c']],
            [['F', '->', 'x', '.', 'F'], ['F', '->', '.', 'x', 'F'], ['F', '->', '.', 'a']],
            [['F', '->', 'a', '.']],
            [['S', '->', 'b', 'E', 'c', '.']],
            [['E', '->', 'F', 'c', '.']],
            [['F', '->', 'x', 'F', '.']]
        ]
        self.assertEqual(self.parser.gen_set_list(), expected_set_list)


if __name__ == '__main__':
    unittest.main()
